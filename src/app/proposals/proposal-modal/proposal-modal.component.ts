import { Component, Inject, Input, OnInit, EventEmitter } from '@angular/core';
import { ProposalService } from '../../services/proposal.service';
import { DriverService } from '../../services/driver.service';
import { Proposal } from '../../models/proposal';
import { Driver } from '../../models/driver';
import { Objective } from '../../models/objective';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import * as _ from 'lodash';

@Component({
  selector: 'proposal-modal',
  templateUrl: './proposal-modal.html',
  styleUrls: ['./proposal-modal.scss'],
  providers: [ProposalService, DriverService, NgbModalRef],  
})
export class ProposalModalComponent implements OnInit {

  proposal: Proposal;
  drivers: Driver[];
  ngbModalRef: NgbActiveModal;  
  onSave = new EventEmitter();
  
  

  constructor( private proposalService: ProposalService, private modal: NgbActiveModal, 
    private driverService: DriverService) {
    this.ngbModalRef = modal;
  }

  ngOnInit(){
    let objective = new Objective();
    this.initializeDrivers(this.proposal);
    if (!_.isNil(this.proposal) && _.isObject(this.proposal.objective)) {
      this.proposal.driver = new Driver();
      objective = this.proposal.objective;
    }else{
      objective.id = this.proposal.driver.objective_id;
    }
    
    this.driverService.list(objective).subscribe(
      drivers => {
        this.drivers = drivers;
      }      
    );
    console.log(this.proposal);
  }

  initializeDrivers(proposal: Proposal){
    if(_.isNil(proposal.driver_id)){
      proposal.driver_id = <any>'';
    }

  }

  save(){
    this.proposalService.save(this.proposal).subscribe(
      proposal => {
        this.onSave.emit();
        this.ngbModalRef.close();
      },
      error => {
        this.onSave.emit(error.json());
      }      
    ); 
  }

}
