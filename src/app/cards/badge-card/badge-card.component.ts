import { Component } from '@angular/core';
import { BadgeService } from '../../services/badge.service';
import { Badge } from '../../models/badge';
import * as _ from 'lodash';

@Component({
  selector: 'cards-badge',
  templateUrl: './badge-card.html',
  styleUrls: ['./badge-card.scss'],
  providers: [BadgeService],  
})
export class BadgeCardComponent {
  badges: Badge[];
  
  constructor( private badgeService: BadgeService ) {      
    this.badgeService.myBadges().subscribe(
      badges => {
        this.badges = _.uniqBy(badges, 'group_id');
      },
    );   
  }

}
