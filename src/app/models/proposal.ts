import { Objective } from './objective';
import { Driver } from './driver';

export class Proposal {
    
    id: number;
    user_id: number;
    title: string;
    description: string;    
    objective: Objective;
    driver: Driver; 
    voting_users_ids: number[];
    driver_id: number; 
}
