import { Component, Input } from '@angular/core';

@Component({
  selector: 'card-base',
  templateUrl: './card-base.html',
})
export class CardBaseComponent {
  @Input() cardTitle: string;
  @Input() baCardClass: string;
  @Input() cardType: string;
}
