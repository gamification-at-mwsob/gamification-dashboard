import { Component } from '@angular/core';

import { NotificationService } from './notification.service';

@Component({
  selector: 'profile-notification',
  providers: [NotificationService],
  styleUrls: ['./notification.scss'],
  templateUrl: './notification.html',
})
export class NotificationComponent {

  notifications: Object[];
  messages: Object[];

  constructor(private notificationService: NotificationService) {
    this.notifications = this.notificationService.getNotifications();
    this.messages = this.notificationService.getMessages();
  }

}
