import { Injectable } from '@angular/core';
import { Proposal } from '../models/proposal';
import { Objective } from '../models/objective';
import { Observable } from 'rxjs/Observable';
import { Restangular } from 'ngx-restangular';

import * as _ from 'lodash';

@Injectable()
export class ProposalService {

  constructor (private restangular: Restangular ) {}

  list (objective: Objective, params?: any): any {
    if (!_.isObject(params)) {
      params = {};
    }

    params['page'] = params['page'] || 1;
    params['per_page'] = params['per_page'] || 5;  
    return this.restangular.one('strategic_objectives', objective.id).all('proposals').getList(params);
  }

  vote (proposal: Proposal): any {   
    let objective_id = _.isObject(proposal.objective) ? proposal.objective.id : proposal.driver.objective_id;
    
    return this.restangular.one('strategic_objectives', objective_id).one('proposals',proposal.id).one('vote').customPOST({});
  }

  save (proposal: Proposal): any {    
    if(proposal.id){
      let objective_id = _.isObject(proposal.objective) ? proposal.objective.id : proposal.driver.objective_id;
      // if(proposal.objective){}
      return this.restangular.one('strategic_objectives', objective_id).one('proposals',proposal.id).customPUT(proposal);
    }else{
      return this.restangular.one('strategic_objectives', proposal.objective.id).all('proposals').customPOST(proposal);
    }    
  }

}
