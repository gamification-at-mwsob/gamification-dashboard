import { Injectable } from '@angular/core';
import { Badge } from '../models/badge';
import { Observable } from 'rxjs/Observable';
import { Restangular } from 'ngx-restangular';
import * as _ from 'lodash';

@Injectable()
export class BadgeService {

  constructor ( private restangular: Restangular ) {}

  list (params?: any): Observable<Badge[]> {        
    if (!_.isObject(params)) {
      params = {};
    }

    params['page'] = params['page'] || 1;
    params['per_page'] = params['per_page'] || 4;    

    return this.restangular.all('badges').getList(params);
  }

  myBadges (): Observable<Badge[]> {
    return this.restangular.all('badges').customGET('my');
  }


}
