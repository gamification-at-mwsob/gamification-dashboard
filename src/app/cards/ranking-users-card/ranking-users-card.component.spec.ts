import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, fakeAsync, tick, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { TranslateModule } from '@ngx-translate/core';
import * as helpers from '../../../spec/helpers';
import { Observable } from 'rxjs/Observable'; 

import { RankingUsersCardComponent } from './ranking-users-card.component';
import { BadgeService } from '../../services/badge.service';

describe('RankingUsersCardComponent', () => {

  let component: RankingUsersCardComponent;
  let fixture: ComponentFixture<RankingUsersCardComponent>;
  const mocks = helpers.getMocks();
  
  // beforeEach(async(() => {    
  //   TestBed.configureTestingModule({
  //     declarations: [RankingUsersCardComponent],
  //     providers: [{ provide: BadgeService, useValue: mocks.badgeService }],
  //     imports: [TranslateModule.forRoot()],
  //     schemas: [CUSTOM_ELEMENTS_SCHEMA],
  //   }).overrideComponent(RankingUsersCardComponent, {
  //     set: {
  //       providers: [
  //         { provide: BadgeService, useValue: mocks.badgeService }],
  //   }});

  //   fixture = TestBed.createComponent(RankingUsersCardComponent);
  //   component = fixture.componentInstance;
  // }));

  // it('should display the list of ranked users', () => {
  //   expect(fixture.debugElement.queryAll(By.css('.ranking-list')).length).toEqual(1);
  // });

});
