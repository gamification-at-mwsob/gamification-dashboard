import { NgModule, ApplicationRef } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { TranslateService } from '@ngx-translate/core';
import { RestangularModule } from 'ngx-restangular';
import { Angular2TokenService, A2tUiModule } from 'angular2-token';
import { Ng2Webstorage } from 'ngx-webstorage';
import { NgPipesModule } from 'ngx-pipes';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';

/*
 * Platform and Environment providers/directives/pipes
 */

import { rootRouterConfig } from './app.routing';

// App is our top level component
import { AppComponent } from './app.component';
import { AppState, InternalStateType } from './app.service';
import { GlobalState } from './global.state';
import { SignOutComponent } from './sign-out/sign-out.component';
import { DashboardComponent } from './layout/dashboard/dashboard.component';
import { LayoutComponent } from './layout/layout.component';
import { ScrollPositionDirective } from './layout/scroll-position.directive';
import { SlimScrollDirective } from './layout/slim-scroll.directive';
import { NavbarComponent } from './layout/navbar/navbar.component';
import { SidebarComponent } from './layout/sidebar/sidebar.component';
import { ContentTopComponent } from './layout/content-top/content-top.component';
import { BackTopComponent } from './layout/back-top/back-top.component';
import { MenuComponent } from './layout/menu/menu.component';
import { MenuItemComponent } from './layout/menu/menu-item/menu-item.component';
import { PlanejamentoEstrategicoComponent } from './planejamento-estrategico/planejamento-estrategico.component';
import { BadgesComponent } from './badges/badges.component';
import { AppTranslationModule } from './app.translation.module';
import { RoundProgressModule } from 'angular-svg-round-progressbar';
// import { HttpClientModule } from '@angular/common/http';
import { ActionsComponent } from './actions/actions.component';
import { ObjectivesComponent } from './objectives/objectives.component';
import { ProposalsComponent } from './proposals/proposals.component';
import { RankingUsersComponent } from './ranking-users/ranking-users.component';
import { RankingProposalsComponent } from './ranking-proposals/ranking-proposals.component';
import { ProposalModalComponent } from './proposals/proposal-modal/proposal-modal.component';
import { ProposalShowModalComponent } from './proposals/proposal-show-modal/proposal-show-modal.component';
import { ValidationMessageComponent } from './shared/validation-message/validation-message.component';
import { ValidateOnBlurDirective } from './shared/validation-message/validate-onblur.directive';


// Services
import { UserService } from './services/user.service';
import { SpinnerService } from './services/spinner.service';
import { PreloaderService } from './services/preloader.service';
import { MessageNotificationService } from './services/message-notification.service';

// App cards
import { ActionCardComponent } from './cards/action-card/action-card.component';
import { BadgeCardComponent } from './cards/badge-card/badge-card.component';
import { CardBaseComponent } from './cards/card-base/card-base.component';
import { RankingUsersCardComponent } from './cards/ranking-users-card/ranking-users-card.component';
import { RankingProposalsCardComponent } from './cards/ranking-proposals-card/ranking-proposals-card.component';

// Profile Actions
import { NotificationComponent } from './profile/notification/notification.component';
import { ProfileImageComponent } from './profile/profile-image/profile-image.component';

// Move componento for projecto root
import { LoginComponent } from './login/login.component';

// Application wide providers
const APP_PROVIDERS = [
  AppState,
  Angular2TokenService,
  GlobalState,
  UserService,
  SpinnerService,
  PreloaderService,
];

export type StoreType = {
  state: InternalStateType,
  restoreInputValues: () => void,
  disposeOldHosts: () => void,
};

export function RestangularConfigFactory(restangularProvider, tokenService: Angular2TokenService) {
  restangularProvider.setBaseUrl('/api');
  restangularProvider.addFullRequestInterceptor((element, operation, path, url, headers, params) => {
    return { headers: Object.assign({}, headers, tokenService.currentAuthHeaders.toJSON()) };
  });

// RestangularProvider.addErrorInterceptor((error, subject, responseHandler) => {
//   let errMsg: string;
//   let errorResponse = {};
//   if (error instanceof Response) {
//     const body = error.json() || '';
//     const err = body.error || JSON.stringify(body);
//     errorResponse['status'] = error.status;
//     errorResponse['status_text'] = error.statusText;
//     errorResponse['body'] = body;
//     errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
//   } else {
//     errMsg = error.message ? error.message : error.toString();
//   }
//   errorResponse['full_message'] = errMsg;
//   console.log("RESTANGULAR ERROR", error);

//   toastr.error("Ocorreu um erro ao executar a operação", "Oops!");
//   return true;
// });
}

/**
 * `AppModule` is the main entry point into Angular2's bootstraping process
 */
@NgModule( {
  bootstrap: [AppComponent],
  declarations: [
    AppComponent,
    SignOutComponent,
    DashboardComponent,
    LayoutComponent,
    NavbarComponent,
    SidebarComponent,
    MenuComponent,
    MenuItemComponent,
    ContentTopComponent,
    BackTopComponent,
    LoginComponent,
    ActionCardComponent,
    BadgeCardComponent,
    RankingUsersCardComponent,
    RankingProposalsCardComponent,
    CardBaseComponent,
    ActionsComponent,
    ObjectivesComponent,
    ProposalsComponent,
    RankingUsersComponent,
    RankingProposalsComponent,
    ProposalModalComponent,
    ProposalShowModalComponent,
    PlanejamentoEstrategicoComponent,
    BadgesComponent,
    NotificationComponent,
    ValidationMessageComponent,
    ValidateOnBlurDirective,
    ProfileImageComponent
  ],
  imports: [ // import Angular's modules
    BrowserModule,
    HttpModule,
    NgPipesModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    // RouterModule,
    RouterModule.forRoot(rootRouterConfig),
    FormsModule,
    ReactiveFormsModule,
    // HttpClientModule,
    // NgaModule.forRoot(),
    NgbModule.forRoot(),
    RestangularModule.forRoot([Angular2TokenService], RestangularConfigFactory),
    Ng2Webstorage.forRoot({ prefix: 'gamification', caseSensitive: true }) ,
    AppTranslationModule,
    RoundProgressModule,    
    // routing,
  ],
  providers: [ // expose our Services and Providers into Angular's dependency injection
    APP_PROVIDERS,
    MessageNotificationService,    
  ],
  entryComponents: [ProposalModalComponent, ProposalShowModalComponent],  
})

export class AppModule {

  constructor(public appState: AppState) {
  }
}
