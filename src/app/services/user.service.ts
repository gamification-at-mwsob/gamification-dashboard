import { Injectable, EventEmitter, Output, OnInit } from '@angular/core';
import { User } from '../models/user';
import { Observable } from 'rxjs/Observable';
import { Restangular } from 'ngx-restangular';
import { LocalStorageService, SessionStorageService } from 'ngx-webstorage';

@Injectable()
export class UserService {
  private user: User;

  @Output() userChangeEvent: EventEmitter<User> = new EventEmitter(true);

  constructor ( private restangular: Restangular, private localStorage: LocalStorageService) {}

  setUser(user: User) {
    this.user = user;
    this.localStorage.store('user', this.user);
    this.userChangeEvent.emit(user);
  }

  getUser(): User {
    this.user = this.localStorage.retrieve('user');
    return this.user;
  }
  
  // FIXME remove this code
  // signIn(user: any): Observable<any> {    
  //   return this.restangular.one('/auth/sign_in').post();
  // }

}
