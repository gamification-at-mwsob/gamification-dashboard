import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, fakeAsync, tick, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { TranslateModule } from '@ngx-translate/core';
import * as helpers from '../../../spec/helpers';
import { Observable } from 'rxjs/Observable'; 
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { UserService } from '../../services/user.service';

import { ProfileImageComponent } from './profile-image.component';

describe('ProfileImageComponent', () => {

  let component: ProfileImageComponent;
  let fixture: ComponentFixture<ProfileImageComponent>;
  const mocks = helpers.getMocks();
  
  beforeEach(async(() => {    
    TestBed.configureTestingModule({
      declarations: [ProfileImageComponent],
      providers: [{ provide: UserService, useValue: mocks.userService }],
      imports: [TranslateModule.forRoot(), RouterTestingModule],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    });
    
    fixture = TestBed.createComponent(ProfileImageComponent);
    component = fixture.componentInstance;
  }));

  
  it('should display the profile image component', () => {
    expect(fixture.debugElement.queryAll(By.css('img')).length).toEqual(1);
  });

});
