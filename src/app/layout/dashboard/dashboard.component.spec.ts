import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, fakeAsync, tick, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { TranslateModule } from '@ngx-translate/core';
import * as helpers from '../../../spec/helpers';
import { Observable } from 'rxjs/Observable'; 

import { DashboardComponent } from './dashboard.component';

describe('BadgesComponent', () => {

  let component: DashboardComponent;
  let fixture: ComponentFixture<DashboardComponent>;
  const mocks = helpers.getMocks();
  
  beforeEach(async(() => {    
    TestBed.configureTestingModule({
      declarations: [DashboardComponent],
      imports: [TranslateModule.forRoot()],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    });
    
    fixture = TestBed.createComponent(DashboardComponent);
    component = fixture.componentInstance;
  }));

  // it('should display the action card on board', () => {
  //   expect(fixture.debugElement.queryAll(By.css('cards-action')).length).toEqual(1);
  // });

  // it('should display the badge card on board', () => {
  //   expect(fixture.debugElement.queryAll(By.css('cards-badge')).length).toEqual(1);
  // });

});
