import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, fakeAsync, tick, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { TranslateModule } from '@ngx-translate/core';
import * as helpers from '../../../spec/helpers';
import { Observable } from 'rxjs/Observable'; 

import { ActionCardComponent } from './action-card.component';
import { ActionService } from '../../services/action.service';

describe('ActionCardComponent', () => {

  let component: ActionCardComponent;
  let fixture: ComponentFixture<ActionCardComponent>;
  const mocks = helpers.getMocks();
  
  beforeEach(async(() => {    
    TestBed.configureTestingModule({
      declarations: [ActionCardComponent],
      providers: [{ provide: ActionService, useValue: mocks.actionService }],
      imports: [TranslateModule.forRoot()],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).overrideComponent(ActionCardComponent, {
      set: {
        providers: [
          { provide: ActionService, useValue: mocks.actionService }],
    }});

    fixture = TestBed.createComponent(ActionCardComponent);
    component = fixture.componentInstance;
  }));

  it('should display the list of action cards', () => {
    expect(fixture.debugElement.queryAll(By.css('.action-list')).length).toEqual(1);
  });

});
