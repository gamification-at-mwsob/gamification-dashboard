import { Component, ViewContainerRef, AfterViewInit } from '@angular/core';
import * as $ from 'jquery';

import { GlobalState } from './global.state';
import { PreloaderService } from './services/preloader.service';
import { SpinnerService } from './services/spinner.service';

import { Angular2TokenService } from 'angular2-token';
import { Router, NavigationEnd } from '@angular/router';
import * as _ from 'lodash';


/*
 * App Component
 * Top Level Component
 */
@Component({
  selector: 'app',
  styleUrls: ['./app.component.scss'],  
  template: `
    <main [class.menu-collapsed]="isMenuCollapsed" [ngClass]="currentPath">
      <div class="additional-bg"></div>
      <router-outlet></router-outlet>
    </main>
  `,
})
export class AppComponent implements AfterViewInit {

  isMenuCollapsed: boolean = false;
  currentPath: string = '';

  constructor(private _state: GlobalState,
              private _spinner: SpinnerService,
              private viewContainerRef: ViewContainerRef,              
              tokenService: Angular2TokenService, private router: Router) {

    tokenService.init({ apiPath: '/api' });

    router.events.subscribe(event => {
      if (event instanceof NavigationEnd ) {
        let url = _.replace(event.url, '/', '');
          
        if (!url) {
          url = 'initial';
        }
        this.currentPath = url;
      }
    });
    this._state.subscribe('menu.isCollapsed', (isCollapsed) => {
      this.isMenuCollapsed = isCollapsed;
    });
  }

  ngAfterViewInit(): void {
    // hide spinner once all loaders are completed
    PreloaderService.load().then((values) => {
      this._spinner.hide();
    });
  }

}
