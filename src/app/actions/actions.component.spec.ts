import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, fakeAsync, tick, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { TranslateModule } from '@ngx-translate/core';
import * as helpers from '../../spec/helpers';
import { Observable } from 'rxjs/Observable'; 

import { ActionsComponent } from './actions.component';
import { ActionService } from '../services/action.service';

describe('ActionsComponent', () => {

  let component: ActionsComponent;
  let fixture: ComponentFixture<ActionsComponent>;
  const mocks = helpers.getMocks();
  const actionService = mocks.actionService;
  
  beforeEach(async(() => {    
    TestBed.configureTestingModule({
      declarations: [ActionsComponent],
      providers: [{ provide: ActionService, useValue: actionService }],
      imports: [TranslateModule.forRoot()],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).overrideComponent(ActionsComponent, {
      set: {
        providers: [
          { provide: ActionService, useValue: actionService }],
    }});

    fixture = TestBed.createComponent(ActionsComponent);
    component = fixture.componentInstance;
  }));

  it('should display the list of actions', () => {
    expect(fixture.debugElement.queryAll(By.css('.action-list')).length).toEqual(1);
  });

});
