import { Component, Inject, Input, OnInit, EventEmitter } from '@angular/core';
import { Proposal } from '../../models/proposal';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ProposalService } from '../../services/proposal.service';
import { MessageNotificationService } from '../../services/message-notification.service';

import { User } from '../../models/user';
import { UserService } from '../../services/user.service';

import * as _ from 'lodash';

@Component({
  selector: 'proposal-show-modal',
  templateUrl: './proposal-show-modal.html',
  styleUrls: ['./proposal-show-modal.scss'],
  providers: [NgbModalRef, ProposalService],  
})
export class ProposalShowModalComponent {

  proposal: Proposal;
  ngbModalRef: NgbActiveModal;  
  onSave = new EventEmitter();
  user: User;
  

  constructor(private messageNotificationService: MessageNotificationService, 
    private proposalService: ProposalService, private modal: NgbActiveModal, 
    private userService: UserService ) {
    this.ngbModalRef = modal;
    this.user = this.userService.getUser();    
    
  }

  back(){
    this.ngbModalRef.close();
  }

  vote(proposal: Proposal){
    this.proposalService.vote(proposal).subscribe( (new_proposal) => {
      console.log('voce votou na proposta ', proposal)
      proposal = Object.assign(proposal, new_proposal);
      this.messageNotificationService.success({ title: "", message: "Seu voto foi computado na proposta." });
    }, (error) =>{
      console.log(error);
      this.messageNotificationService.error({ title: "", message: 'Ocorreu um erro ao tentar salvar seu voto e ele não foi salvo.'});
    }); 
  }

  isOwner(proposal: Proposal){
    return proposal.user_id === this.user.id;
  }

  alreadyVoted(proposal: Proposal){
    return _.includes(proposal.voting_users_ids, this.user.id);
  }

}
