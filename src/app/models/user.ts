import { Level } from './level';


export class User {
    id: number;
    email: string;
    password: string;
    login: string;
    name: string;
    points: number;
    badge_ids: number[];
    level: Level;
    passwordConfirmation: string;
    
}
