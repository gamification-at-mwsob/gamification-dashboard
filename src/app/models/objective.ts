export class Objective {
    
    id: number;
    title: string;
    description: string;    
    
}
