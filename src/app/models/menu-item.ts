export class MenuItem {
    
    title: string;
    url: string;
    selected: boolean;
    hidden: boolean;
    icon: string;
    expanded: boolean;
    path: string | string[];
    children: MenuItem[];
    
}
