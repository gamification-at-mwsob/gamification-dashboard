import { Component, Inject } from '@angular/core';
import { ActionService } from '../services/action.service';
import { Action } from '../models/action';

@Component({
  selector: 'actions',
  templateUrl: './actions.html',
  styleUrls: ['./actions.scss'],
  providers: [ActionService],  
})
export class ActionsComponent {

  actions: Action[];

  constructor( private actionService: ActionService ) {
    const params = { 'page': 1, 'per_page': 50 };
    this.actionService.list(params).subscribe(
      actions => {
          this.actions = actions;
      },
    );    
  }
}
