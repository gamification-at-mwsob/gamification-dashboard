import { Routes } from '@angular/router';
import { SignOutComponent } from './sign-out/sign-out.component';
import { DashboardComponent } from './layout/dashboard/dashboard.component';
import { LayoutComponent } from './layout/layout.component';
import { ActionsComponent } from './actions/actions.component';
import { RankingUsersComponent } from './ranking-users/ranking-users.component';
import { RankingProposalsComponent } from './ranking-proposals/ranking-proposals.component';
import { ObjectivesComponent } from './objectives/objectives.component';
import { ProposalsComponent } from './proposals/proposals.component';
import { PlanejamentoEstrategicoComponent } from './planejamento-estrategico/planejamento-estrategico.component';
import { BadgesComponent } from './badges/badges.component';
import { LoginComponent } from './login/login.component';
import { Angular2TokenService } from 'angular2-token';

export const rootRouterConfig: Routes = [
  {
    path: '', component: LoginComponent,
  },
  {
    path: 'dashboard', component: LayoutComponent, canActivate: [Angular2TokenService],
    children: [
      { path: '', component: DashboardComponent, canActivate: [Angular2TokenService] },
      // { path: 'editors', loadChildren: './editors/editors.module#EditorsModule' },
    ],
  },
  {
    path: 'actions', component: LayoutComponent, canActivate: [Angular2TokenService],
    children: [
      { path: '', component: ActionsComponent, canActivate: [Angular2TokenService] },
    ],
  },
  {
    path: 'users', component: LayoutComponent, canActivate: [Angular2TokenService],
    children: [
      { path: '', component: RankingUsersComponent, canActivate: [Angular2TokenService] },
    ],
  },
  {
    path: 'proposals', component: LayoutComponent, canActivate: [Angular2TokenService],
    children: [
      { path: '', component: RankingProposalsComponent, canActivate: [Angular2TokenService] },
    ],
  },
  {
    path: 'objectives',
    
              component: LayoutComponent, 

      children: [
        // { 
          // path: '', 
          // component: LayoutComponent, 
          // canActivate: [Angular2TokenService],
          // children: [
            {
              path: ':objective',
              component: ProposalsComponent
            },
            {
              path: ':objective/proposals',
              component: ProposalsComponent
            },
          // ]
      // },
    ],
  },
  {
    path: 'badges', component: LayoutComponent, canActivate: [Angular2TokenService],
    children: [
      { path: '', component: BadgesComponent, canActivate: [Angular2TokenService] },
    ],
  },
  {
    path: 'planejamento-estrategico', component: LayoutComponent, canActivate: [Angular2TokenService],
    children: [
      { path: '', component: PlanejamentoEstrategicoComponent, canActivate: [Angular2TokenService] },
    ],
  },
  { path: 'sign-out', component: SignOutComponent },
];
